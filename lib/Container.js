'use strict';

const express = require('express'),
	path = require('path');

/**
 * Container for keeping track of dependencies
 */
class Container {

	/**
	 * Create the object container
	 *
	 * @constructor
	 */
	constructor() {
		const app = express();
		this._container = {
			app: app,
		};
	}

	/**
	 * Determine if an item exists in the container
	 *
	 * @param  {String}  name - name of the item
	 * @return {Boolean} - whether the item exists in the container
	 */
	has(name) {
		return (name in this._container);
	}

	/**
	 * Return an existing object instance
	 *
	 * @param  {String} name - name of the item
	 * @return {Object|Null} - the item, or null if it doesn't exist
	 */
	get(name) {
		if (! this.has(name)) {
			return null;
		}

		return this._container[name];
	}

	/**
	 * Set an object in the container
	 *
	 * @param {String} name - name to associate with object
	 * @param {Object} object - item to keep track of
	 * @return {Container} - the container instance
	 */
	set(name, object) {
		this._container[name] = object;
		return this;
	}

	/**
	 * Does a native require, relative to the lib folder,
	 * and returns the value
	 *
	 * @param  {String} modulePath - name of the module to require
	 * @return {mixed} - the value returned from require
	 */
	require(modulePath) {
		// If the value is already saved, just return it
		if (this.has(modulePath)) {
			return this.get(modulePath);
		}

		let includeName = modulePath;

		// Allow referencing local modules without using a ./
		// eg. util/route-loader instead of ./util/route-loader
		if (
			modulePath.includes('/') &&
			! (modulePath.startsWith('./') || modulePath.includes(__dirname))
		) {
			includeName = path.join(__dirname, modulePath);
		}

		return require(includeName);
	}
}

let instance = null;

module.exports = (function () {

	if (instance === null) {
		instance = new Container();
	}

	return instance;
}());