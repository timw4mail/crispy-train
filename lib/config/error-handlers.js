'use strict';

// -----------------------------------------------------------------------------
// Error handlers
// -----------------------------------------------------------------------------

const container = require('../Container');
const app = container.get('app');

let errorHandlers = [

	// catch 404 and forward to error handler
	(req, res, next) => {
		let err = new Error('Not Found');
		err.status = 404;
		next(err);
	},

	// general error handler
	(err, req, res, next) => {
		res.status(err.status || 500);

		let output = {
			status: err.status,
			message: err.message,
		};

		// Show stack trace in development environment
		if (app.get('env') === 'development') {
			output.error = err;
		}

		res.json(output);
	},
];

module.exports = errorHandlers;