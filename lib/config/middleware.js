'use strict';

// -----------------------------------------------------------------------------
// Middleware
// -----------------------------------------------------------------------------

const bodyParser = require('body-parser'),
	cookieParser = require('cookie-parser'),
	express = require('express'),
	helmet = require('helmet'),
	requestLogger = require('morgan'),
	path = require('path');

let middleware = [

	// some security settings controlled by helmet
	helmet.frameguard(),
	helmet.hidePoweredBy(),
	helmet.ieNoOpen(),
	helmet.noSniff(),
	helmet.xssFilter(),

	// basic express middleware
	requestLogger('combined'),
	bodyParser.json(),
	bodyParser.urlencoded({ extended: false }),
	cookieParser(),
	express.static(path.join(__dirname, '../../public')),
];

module.exports = middleware;