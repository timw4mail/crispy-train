'use strict';

let nconf = require('nconf');

/**
 * Config management class
 */
class Config {
	constructor() {
		// Use, in order of preference:
		// - command line arguments
		// - environment variables
		// - config files
		nconf.argv()
			.env()
			.file({

			});
	}

	/**
	 * Retreive a config value
	 *
	 * @param  {String} key - the name of the config value
	 * @return {mixed} - the configuration value
	 */
	get(key) {

	}
}

module.exports = Config;