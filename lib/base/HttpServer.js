'use strict';

const http = require('http'),
	logger = require('winston');

/**
 * Class for creating an http server
 *
 * @param {Express} app - current express instance
 * @param {Number} port - the port to listen on
 */
class HttpServer {
	/**
	 * Creates an HTTP Server
	 *
	 * @constructor
	 * @param {Express} app - current express instance
	 * @param {Number} port - the port to listen on
	 */
	constructor(app, port) {
		let server = http.createServer(app);
		server.listen(port);
		server.on('error', this.onError);
		server.on('listening', () => {
			let addr = server.address();
			let bind = typeof addr === 'string'
				? `pipe ${addr}`
				: `port ${addr.port}`;
			logger.info(`Listening on ${bind}`);
		});

		this.server = server;
	}

	/**
	 * Event listener for HTTP server "error" event.
	 *
	 * @param {Error} error - the error object
	 * @return {Null} - Does not return a value
	 * @throws {Error}
	 */
	onError(error) {
		if (error.syscall !== 'listen') {
			throw error;
		}

		let port = this.server.address().port;

		let bind = typeof port === 'string'
			? `Pipe ${port}`
			: `Port ${port}`;

		// handle specific listen errors with friendly messages
		switch (error.code) {
			case 'EACCES':
				logger.error(`${bind} requires elevated privileges`);
				process.exit(1);
				break;
			case 'EADDRINUSE':
				logger.error(`${bind} is already in use`);
				process.exit(1);
				break;
			default:
				throw error;
		}
	}
}

module.exports = HttpServer;