'use strict';

const container = require('./Container');
const loadRoutes = container.require('util/route-loader'),
	path = container.require('path');

const app = container.get('app');
const middleware = container.require('config/middleware');
const routes = loadRoutes(path.join(__dirname, 'routes'));
const errorHandlers = container.require('config/error-handlers');

// load middleware
middleware.forEach((mw) => app.use(mw));

// automatically set up routing by folder structure
Object.keys(routes).reverse().forEach((path) => {
	app.use(path, require(routes[path]));
});

// load error handlers
errorHandlers.forEach((handler) => app.use(handler));

module.exports = app;