'use strict';

const glob = require('glob');

/**
 * Map Routes to route files
 *
 * @param  {String} path - folder with Routes
 * @return {Object} - Object mapping routes to their files
 */
module.exports = function routeLoader(path) {

	const basePath = path;

	let paths = glob.sync(`${path}/**/*.js`);
	paths = paths.sort();

	let routes = {};

	paths.forEach((path) => {
		let routePath = path.replace(basePath, '')
			.replace('.js', '')
			.replace('index', '');

		routes[routePath] = path;
	});

	return routes;
};