'use strict';

const path = require('path');

/**
 * Base Object for unit test utilities
 */
const testBase = {

	/**
	 * Chai expect assertion library
	 */
	expect: require('chai').expect,

	/**
	 * Determine the appropriate path to a module relative to the root folder
	 *
	 * @param  {String} modulePath - the raw path to the module
	 * @return {String} - the normalized path to the module
	 */
	_normalizeIncludePath(modulePath) {
		const basePath = path.resolve(path.join(__dirname, '../'));

		let includePath = modulePath;

		// Allow referencing local modules without using a ./
		// eg. util/route-loader instead of ./util/route-loader
		if (modulePath.includes('/') && ! modulePath.startsWith('./')) {
			includePath = path.join(basePath, modulePath);
		}

		return includePath;
	},

	/**
	 * Load a module relative to the root folder
	 *
	 * @param  {String} modulePath - path to the module, relative to the tests/ folder
	 * @return {mixed} - whatever the module returns
	 */
	testRequire(modulePath) {
		const includePath = testBase._normalizeIncludePath(modulePath);
		return require(includePath);
	},

	/**
	 * Load a module relative to the root folder, but first delete
	 * the module from the require cache
	 *
	 * @param  {String} modulePath - path to the module, relative to the tests/ folder
	 * @return {mixed} - whatever the module returns
	 */
	testRequireNoCache(modulePath) {
		const includePath = testBase._normalizeIncludePath(modulePath);
		delete require.cache[includePath];
		return require(includePath);
	},
};

module.exports = testBase;