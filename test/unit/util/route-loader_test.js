'use strict';

const expect = require('chai').expect,
	path = require('path');
let routeLoader = require(path.join(__dirname, '/../../../lib/util/route-loader'));

suite('Util - Route Loader', () => {
	test('routeLoader creates accurate route mapping', () => {
		let actual = routeLoader(path.join(__dirname, 'test-routes'));
		let expected = {
			'/api/foo/bar': path.join(__dirname, 'test-routes/api/foo/bar.js'),
			'/api/foo': path.join(__dirname, 'test-routes/api/foo.js'),
			'/': path.join(__dirname, 'test-routes/index.js'),
		};

		expect(expected).to.be.deep.equal(actual);
	});
});