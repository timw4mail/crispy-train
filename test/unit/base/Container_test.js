'use strict';

const testBase = require('../../test-base');
const expect = testBase.expect;

suite('Dependency Container tests', () => {
	let container = null;

	setup(() => {
		// Delete cached version of container class that may have been required
		// at an earlier time. The container module has a saved state to keep track
		// of modules during normal use. Removing the cached version ensures that
		// a new instance of the module is used.
		container = testBase.testRequireNoCache('lib/Container');
	});

	test('Multiple requires return the same instance', () => {
		let container2 = testBase.testRequire('lib/Container');
		expect(container2).to.be.equal(container);
	});

	suite('has method', () => {
		setup(() => {
			container.set('foobar', {
				foo: {
					bar: [1, 2, 3],
				},
			});
		});

		test('Item "foobar" exists', () => {
			expect(container.has('foobar')).to.be.true;
		});

		test('Item "abc" does not exist', () => {
			expect(container.has('abc')).to.be.false;
		});
	});

	suite('Get/set functionality', () => {
		let obj = {
			foo: {
				bar: [1, 2, 3],
			},
		};

		test('Set method returns Container', () => {
			let actual = container.set('foobar', obj);
			expect(actual).to.be.equal(container);
		});

		test('Get method returns set object', () => {
			let actual = container.get('foobar');
			expect(actual).to.be.equal(obj);
		});

		test('Attempt to get non-existent item returns null', () => {
			expect(container.get('aseiutj')).to.be.null;
		});
	});

	suite('require method', () => {
		test('Returns same object as testInclude', () => {
			let containerFile = container.require('app');
			let testFile = testBase.testRequire('lib/app');

			expect(containerFile).to.be.equal(testFile);
		});

		test('Returns same object as native require', () => {
			let containerFile = container.require('express');
			let nativeRequire = require('express');

			expect(containerFile).to.be.equal(nativeRequire);
		});
	});
});