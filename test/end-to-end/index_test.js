'use strict';

const supertest = require('supertest'),
	expect = require('chai').expect;

let server = supertest.agent('http://localhost:3000');

suite('Example tests', () => {
	test('Index page works as expected', (done) => {
		server.get('/')
			.expect('Content-type', /json/)
			.expect(200)
			.end((err, res) => {
				expect(res.status).to.equal(200);
				expect(res.body.error).to.be.undefined;
				expect(res.body).to.deep.equal({
					status: 200,
					data: {
						index: { title: 'Express' },
					},
				});
				done();
			});
	});

	test('404 page works as expected', (done) => {
		server.get('/foo')
			.expect('Content-type', /json/)
			.expect(404)
			.end((err, res) => {
				expect(res.status).to.equal(404);
				expect(res.body.status).to.equal(res.status);
				expect(res.body.message).to.be.ok;

				let expected = {
					status: 404,
					message: 'Not Found',
				};

				if (process.env.NODE_ENV === 'development') {
					expected.error = {
						status: 404,
					};
				}

				expect(res.body).to.deep.equal(expected);

				done();
			});
	});
});