'use strict';

const container = require('./lib/Container');
const app = container.require('./app'),
	logger = require('winston'),
	HttpServer = container.require('base/HttpServer');

/**
 * Normalize a port into a number, string, or false.
 *
 * @private
 * @param {mixed} val - port value
 * @return {mixed} - normalized value
 */
function normalizePort(val) {
	let port = parseInt(val, 10);

	if (isNaN(port)) {
		// named pipe
		return val;
	}

	if (port >= 0) {
		// port number
		return port;
	}

	return false;
}


// Get port from environment and store in Express.
let port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

// Create HTTP Server
let server = new HttpServer(app, port);